/**
******************************************************************************
* @file           : common.h
* @brief          : Header file of face detection
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdbool.h>




// public
extern char gw_serial[12];
extern char node_serial[12];


#endif
