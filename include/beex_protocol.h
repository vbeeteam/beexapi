/**
******************************************************************************
* @file           : beex_protocol.h
* @brief          : Header file of Beex protocol
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef BEEX_PROTOCOL_H
#define BEEX_PROTOCOL_H

#include <stdint.h>
#include <stdbool.h>

#define BEEX_BUFFER_PACKET_MAX              150
#define BEEX_BODY_PACKET_MAX                100
#define BEEX_MAGIC_PACKET                   0x4443

typedef struct{
    char dev_serial[12];
    uint16_t dev_type;
    uint16_t serv_type;
    uint16_t body_len;
    uint8_t body[BEEX_BODY_PACKET_MAX];
    uint32_t timestamp;
} BeexPacket;


#endif
