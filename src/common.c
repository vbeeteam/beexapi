/**
******************************************************************************
* @file           : common.c
* @brief          : Source file of face detection
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "common.h"

char gw_serial[12];
char node_serial[12];


bool initDevice(char *p_gw_serial, char *p_node_serial){
    for (uint8_t i = 0; i < 12; i++){
        gw_serial[i] = p_gw_serial[i];
        node_serial[i] = p_node_serial[i];
    }
    return true;
}
